# React学习路径

- 目标：学习React的各种架构环境，有能力玩转任何一套架构。

- Webpack环境：webpack(v5)、react(v17)、react-router-dom(v5)、mobx(v6)
- create-react-app环境：redux、ant-design、react-router-dom(v5)、项目实战
- dva/umi/ant-design-pro环境：阿里生态开源工具、TypeScript、redux-saga

- 扩展1：vite搭建react环境、create-react-app搭建TS环境(@redux/toolkit)
- 扩展2：webpack搭建vue环境，搭建vue装饰器开发环境。
- 扩展3：Taro环境、uniapp环境、Angular环境。。。

# Webpack

- 为什么要学习Webpack？
  - 前端工程化离不开webpack，它是一个非常经典的构建工具。类似的构建工具，还有vite、gulp、rollup等。
  - webpack通常代表着一种架构能力。

- 怎么学webpack？
  - 核心概念：入口、出口、本地服务、loaders、plugins、Babel、ESlint、优化技巧。
  - 反复练习：使用webpack定制搭建各种你需要的开发环境。
  - 使用文档：勤翻阅webpack及其生态资源文档。

- 如何理解webpack？（官网首页上的那张图）
  - webpack是一个打包器(打包工具、构建工具)，用于把各种资源模块进行打包，变成浏览器能够普遍兼容的静态资源。所以有了webpack，我们就可以环境使用各种新语法。
  - 在webpack眼中，一切皆模块。有了webpack，我们可以非常方便地使用各种模块.js/.jsx/.ts/.tsx/.vue/.scss/.png，还可以方便地使用模块化语法进行开发。
  - 你可以把webpack类比成一个榨汁机，你向榨汁机中丢入不同的原材料，最终打包后得到不同口感的果汁或饮料。所以，榨汁机必须得用入口和出口。

# 开始搭建react开发环境

- 版本说明：node(v14+)、webpack(v5)
- 工具：NPM官网 https://www.npmjs.com/
- 包管理器：yarn、cnpm、npm

- `cnpm i webpack -g` 全局安装
- `cnpm i webpack -D`  提供了webpack核心API及内置插件
- `cnpm i webpack-cli -g` 全局安装
- `cnpm i webpack-cli -D` 提供了webpack命令行工具

- `cnpm i webpack-dev-server -g` 全局安装
- `cnpm i webpack-dev-server -D` 构建本地开发服务器

- `cnpm i html-webpack-plugin -D` 把入口文件和public/index.html关联起来

- 面试题：为什么webpack打包，要给模块文件加chunkhash值？
  - 第1次部署，dist目录放到云服务器上；
  - 用户访问你的网站，请求这些静态资源，浏览器会缓存这些静态资源。
  - 加需求、改需求，本质上就是改代码；再打包时hash发生变化，dist中文件名发生变化，第2次部署。
  - 用户再访问网站，发现文件名变化了，浏览器重新请求新资源来替换本地缓存资源，所以用户就看到了你加的需求。

- `cnpm i webpack-merge -D`  专门用于webpack配置选项的合并
- `cnpm i cross-env -D` 向Node.js进程中添加环境变量（在v5中使用--env选项来添加环境变量）

- `cnpm i file-loader -D` 在v4中用得比较多，用于加载处理图片模块。
- 在v5中， 我们使用 `type:'asset/resource'` 来处理图片模块。

- `cnpm i sass-loader -D` 加载.scss文件的
- `cnpm i sass -D`  用于编译sass文件，得到css文件
- `cnpm i css-loader -D`  加载.css文件，返回css代码
- `cnpm i style-loader -D`  通过DOM操作把css插入到DOM中
- `cnpm i mini-css-extract-plugin -D`  为每一个js文件创建一个独立的.css文件，目的是把js中的css相关的DOM功能抽离出来。

- `cnpm i babel-loader -D` 这是webpack的loader，用于加载各种JS文件。
- `cnpm i @babel/core -D`  Babel核心编译器，无须在babel.config.js中配置，但必须要安装。
- `cnpm i @babel/preset-env -D` 是编译ES6语法的Babel预设，要配置。
- `cnpm i @babel/plugin-proposal-decorators -D` 编译装饰器语法的Babel插件，要配置。
- `cnpm i @babel/plugin-proposal-class-properties -D` 编译装饰器语法的Babel插件，要配置。

- `cnpm i @babel/preset-react -D` 这是JSX语法的Babel编译器，要在babel.config.js中配置。
- `cnpm i react -S`  这是React的核心包。
- `cnpm i react-dom -S`  这是用于渲染React组件的DOM包。

- `cnpm i eslint-loader -D`  这在v4架构中经常用到的，用于集成ESLinit代码检测。咱们这个v5环境不用它了。
- `cnpm i eslint-webpack-plugin -D` 这是一个webpack插件，用于在v5的webpack环境中集成ESLint代码检测。
- `cnpm i eslint -D` 这是ESLint官方提供的代码检测工具，是真正用于检测代码规则的包。

- `cnpm i babel-eslint -D`  这在v4架构中经常用到，现在v5中不推荐使用了。
- `cnpm i @babel/eslint-parser -D` 用于兼容Babel环境对ESLint的检测，非常重要，几乎所有的webpack环境都用到了。
- `cnpm i eslint-plugin-react -D` 这是ESlint插件，用于检测React语法规范的。
- `cnpm i eslint-plugin-react-hooks -D` 这是ESlint插件，用于检测Hooks语法规范的。
- `cnpm i eslint-plugin-jsx-a11y -D` 这是ESlint插件，用于检测JSX属性语法规范的。
- `cnpm i eslint-plugin-import -D` 这是ESlint插件，用于检测模块化语法规范的。
- `eslint-config-airbnb` 这是爱彼迎公司开源的ESlint插件，一步到位地支持ES6、React开发的代码检测。如果你使用了 `eslint-config-airbnb`，上面那一堆ESlint插件就没有必要再安装了。

- 使用Webpack如何搭建React环境？你大约的思路是怎样的？
  - 准备工作：node v14 、webpack v5、npm yarn
  - 基本配置：入口 、出品、webpack-dev-server(hot\proxy)、webpack-merge(--env)、html-webpack-plugin(js+html)
  - 模块编译：file-loader、sass-loader/style-loader/css-loader/抽离css，babel-loader, @babel/*, ba...
  - 代码测试：eslint、@babel/eslint-plugin、hooks、jsx、reactwad...(airbnb-config), .eslintrc.js
  - 优化配置：抽离vendor、hash值、resolve、devtool、mode..
- 你常用的配置有哪些？常用的loaders、plugins有哪些？
- 什么是Babel？你常用的Babel编译器有哪些？为什么要使用Babel？
- 什么是ESLint？你如何集成ESLint？常见的ESLint检测工具有哪些？
- Webpack优化（速度提升优化、代码质量优化）？
  - 抽离vendor、加hash、使用mode、抽离css、exclude忽略第三方包的编译和ESLint检测、devtool、代码分割。。。
  - 查阅文章、把你搞明白的，写进来。。。补充
- 如何自定义封装loader、自定义封装plugin？
- 什么是代码分割？什么是Tree-Shaking？
- 使用webpack，如何自已搭建vue开发环境？你的思路是怎样的？
- 搜索：webpack其它面试！！！先整理！！

# React基础

- 3月29日正式发布了React(v18)：react-dom（并发渲染）、SSR更强健壮、新增若干个Hooks、严格模式、Suspense。。。
- 知识点讲解：主要以v17语法，提及v18。
- 项目实战：以v17为准。

- 一、什么是JSX？如何理解JSX？为什么要使用JSX？

  - 提示：学好JSX，相当于学好了React。换句话，如果你的JSX写不好，你做不了React开发。
  - 凡是用到JSX语法的作用域，都必须导入React，原因是JSX语法会被编译成React.createElement()。
  - 什么是JSX？是React官方提供一种语法糖，JavaScript XML，像编写XML一样地使用React组件。
  - 在React开发中，JSX语法是可选的，也就说你可以不使用JSX语法。如果不使用JSX语法，你必须得使用React.createElement()来编写组件视图，这会非常麻烦，并且代码的可阅读性较差，也难以维护。
  - 语法：React.createElement(type,props,children)，返回React元素（元素可以被渲染成DOM）。
  - JSX和 React.createElement() 到底是什么关系？ 浏览器默认是不支持JSX语法, 使用@babel/preset-react这个编译器, 可以把JSX语法编译成浏览器能够读懂的 React.createElement() 语法。
  ```
  const view = React.createElement(
    'div',
    { className: 'box', id: 'box', title: 'box' },
    [
      React.createElement('h1',{ key:1 },'你好'),
      React.createElement('h2',{ key:2 },'他好'),
      React.createElement(
        'div',
        { title: '呵呵', key:3 },
        React.createElement('span',{},'学习')
      )
    ]
  )
  const view = (
    <div className='box' title='box' id='box'>
      <h1>你好</h1>
      <h2>他好</h2>
      <div title='呵呵'>
        <span>学习</span>
      </div>
    </div>
  )
  ```

- 二、详解JSX的各种语法细节

  - JSX是变量，也是Object对象，还是表达式。JSX是可以像HTML一样地无限嵌套的。
  - 在JSX中还可以使用表达式，但表达式得用 {} 包裹起来。记住，只要在JSX中使用变量或表达式，都要用 {} 包起来。{} 是JSX中一种特殊的语法。
  - 只要是被 {} 包裹过，都可以阻止XSS注入攻击。
  - 如果一个变量是布尔值、null、undefined，在JSX渲染时会忽略它，不参与渲染。
  - JSX中有三个变化的属性：className(class)、tabIndex(tabindex)、htmlFor(for)。
  - JSX中新增了三个属性：key、ref、dangerouslySetInnerHTML(相当于vue中的v-html)。
  - 在JSX中是可以添加注释的，但注释要用 {/* 你的注释 */} 包起来。
  - JSX语法要求，React组件名必须满足大驼峰命名法（即首字母也要大写）。常识：在React代码中，凡是组件其名称的首字母都要大写；如果一个函数其首字母是小写，我们认为它就是一个普通的方法，不是组件。
  - 在JSX中，无论是自定义封装的React组件，还是普通HTML标签。如果其内部没有子元素，我们推荐使用单闭合标签，比如：`<div />`。
  - 在JSX中，元素属性的值如果是动态的，要用 {} 包起来。如果属性值是对象，也要用 {} 包起来。比如：`<div id={ ('box'+1) } style={ {width:'100px', height:'200px'} } />`。
  - 在JSX中，如果一个属性的属性值是`true`，可以省略掉值，比如：`<H checked />`。
  - 什么是React组件(React类)？什么是React元素(JSX元素)？组件是类(蓝图)，元素是组件实例化的结果。比如：`H` 是组件，`<H/>`是元素(`React.createElement(H,{},[])`)。
  - JSX默认就支持对数组的直接渲染，这是JSX语法。需要注意的是，渲染数组时通常需要加Key。
  - JSX元素（React元素）是不可变对象，也就是说：你不能setter一个JSX变量。所以以后我们要改变视图，不能直接修改JSX元素，而应该使用state来控制视图的变化。为什么要使用state来控制视图变化？这可以减少没有必须要的DOM更新。


- 三、两种组件：函数式组件、类组件

  - 函数式组件：用function或箭头函数来定义，没有state、没有this、没有生命周期、没有ref、没有上下文。有props，可以使用Hooks API，并且性能更好。
  - 类组件：用class关键字来定义，有state、有this、有生命周期、有ref、有上下文。也有props、不可以使用Hooks API，并且性能相对较差。
  - 趋势：类组件用得越来越少，函数式组件用得越来越多，所以Hooks编程已经成为主流。

- 四、Props（组件化、非常重要）

  - 约定1：当我们谈论props时，一般说的是自定义组件的属性。
  - 约定2：当我们谈论组件关系（父子、兄弟）时，一般指的是自定义组件，不包括HTML元素。
  - 自定义props可以传递这些数据：基本数据类型、引用数据类型、函数方法、JSX元素。
  - 约定3：当我们设计自定义属性时，如果属性值是一个函数，我们建议这个props属性名以'on'开头，像这样'onRun'。
  - props.children 代表的是自定义组件内部所嵌套“节点们”。
  - props.children 可能是一个数组，也有可能是一个渲染函数。
  - 到底什么是props？props是父子组件之间数据通信的纽带，props是父组件作用域中传递过来的数据。props是组件封装（组件化）中非常重要的技术点。
  - props是只读的，在写业务逻辑的过程中， 我们不能修改props。所以，我们函数式组件是函数，也是纯函数。
  - props属性验证：推荐使用 `prop-types` 这第三方工具来完善props的设计。对非必填属性，一般得给默认值，当然也需要给类型；对必填属性一定要给类型，但不用给默认值。（string/number/element/bool/func/oneOf/oneOfType/array/object）。
  - render props，封装组件时，让 render属性等于一个可以参与子组件视图渲染的函数，比如：`<A redner={()=>element} />`。它和 props.children (function类型) 有异曲同工之妙。

- 五、事件绑定

  - 合成事件：在React中的各种事件，比如鼠标事件、键盘事件、表单事件、浏览器事件等，都被React重新封装过，这些事件都以 on* 开头，比如：onClick、onKeyUp、onChange、onScroll等。
  - 约定：在React中绑定事件，建议使用合成事件的方式进行绑定，举例：`<div onClick={handler} />`。
  - 用ES5语法绑定事件：`<div onClick={this.handler.bind(this)} />`（提示：尽量少用）
  - 用ES6语法绑定事件：`<div onClick={()=>this.handler()} />`（这是我们推荐的写法）
  - 补充：表单事件绑定、事件对象、阻止冒泡、事件传参，这些知识点在“列表渲染和受控表单”中会理次讲到。

- 六、State（声明式）

  - state是组件自有的声明式变量。声明式变量可以参与视图渲染，当声明式变量发生变化时，视图自动更新。
  - 强调：只有类组件才有state，函数式组件默认是没有state的（函数式组件要想拥有state，要使用Hooks来模拟）。
  - 如何定义state？在类组件中，建议在 `constructor()` 这个生命周期中，使用 this.state = {} 定义声明式变量。
  - 如何使用state？在类组件中，使用 `this.state` 来访问这些声明式变量，这些声明式变量可以参与视图渲染。
  - 如何修改state？在类组件中，使用 `this.setState()` 这个专属API来修改state，意思说你不能直接修改state。
    - this.setState() 用于修改state声明式变量，修改成功后会自动触发`render()`执行，只有`render()`重新执行了，才会生成新的虚拟DOM（Fiber树），才会进一步执行diff运算（协调运算），最终才能实现视图的更新。
    - this.setState() 在V18中，这个方法都是异步的；在V17中，可能是异步的，也可能是同步的；在合成事件中是异步的，在定时器、Promise.then()是同步的。
    - this.setState({}|fn, callback) 这个callback表示`this.setState()`这个异步任务执行完成了。
    - 为什么React官方在设计`this.setState()`时要搞成异步的？这可以实现在合成事件作用域里，对多个`this.setState()`进行合并，进而只执行一次`render()`，这样设计的目的是为了性能优化。
    - `this.setState({}, callback)`，当我们要修改的声明式变量，其新值与旧值完全无关时，也就是说新值不是由旧值计算而来时，推荐使用这种语法。
    - `this.state((state,props)=>({}), callback)`，当我们要修改的声明式变量，其新值与旧值有关系时，也就是说新值由旧值计算而来时，推荐使用这种语法。
  - 如何理解State？State是组件自有的声明式数据，当state被`this.setState()`异步地修改时，这会触发视图更新；State只能自上而下地在组件树中传递，也就是说State数据可以通过props向后代传递。

- 七、生命周期

  - 强调：只有类组件才有生命周期，函数式组件没有生命周期。现在React开发一般都是Hooks编程，所以生命周期几乎不用了，但我们仍然要学习生命周期，这是React的基本知识。
  - 口令：三个阶段（装载阶段、更新阶段、卸载阶段）、两个时期（render时期、commit时期）。

  - 三个阶段（3-2-1）
    - 装载阶段(3)：constructor / redner / componentDidMount
    - 更新阶段(2)：[shouldComponentUpdate] render / componentDidUpdate
    - 卸载阶段(1)：componentWillUnmount

  - 两个时期

    - render时期：React.createElement()调用，返回一个又一个的 Fiber({type, props:{children}})，使用“三个指针”（parent/sibling/child）把所有单个的Fiber串联起来，形成了所谓的“Fiber树”。render时期，主要工作就是生成Fiber树，这个过程是异步的，随时可被浏览器中断。有且仅有当浏览器主线程有空闲时，render阶段继续执行；反之，当浏览器比较忙时，render时期将会暂停。
    - commit时期：把Fiber树提交，一次性更新到DOM树中，视图就更新了。commit时期，是同步的，不能被中断的。

  - 补充：当调用this.setState()，会触发render()再执行，生成一棵完整的新的Fiber树，那么现在就有了两棵Fiber树，接着执行协调运算（Diff运算），找到所有的脏的Fiber（也串联成Fiber树），接着一次性commit提交更新DOM。

  - 补充：在React中，on*系列事件、生命周期钩子函数，都是合成事件。

- 八、条件渲染、动态样式、列表渲染

  - 单一节点的条件渲染：`{ bool && <jsx /> }`，`<jsx style={{display:(bool?'block':'none')}/>`。
  - 两个节点的条件渲染：`{ bool ? <jsx1 /> : <jsx2 /> }`
  - 多个节点的条件渲染：建议封装自定义渲染函数来实现，命名约定 `renderSomething() {}`。

  - 动态className 语法参考： `<div className={`box ${cc}`} />`。
  - 动态style 语法参考：`<div style={ {cssKey: `${cssVal}`, color:'red'} } />`。

  - 列表渲染，官方推荐使用 map(callback)，原因 map()返回值就是数组，正好JSX语法也支持直接渲染数组。所以我们在使用 map()方法渲染列表时，只用设计好callback的返回值即可。需要注意的是，除了map()，其它任何可以用于循环的方法，实际上都可以用来实现列表渲染。
  - 列表的Key值，一般用那些“不会变的并且是唯一的”数据，千万不能数组循环时的索引号。

  - 补充：事件传参语法参考 `<div onClick={ev=>this.handler(1,ev,2)} />`。如果在事件处理器函数中，需要阻止冒泡、阻止默认事件或者使用到其它事件对象的API，参考二阶段中事件API的使用。


- 九、表单绑定（受控表单）

  - 受控表单：表单的 value属性或者 checked属性由 state声明式变量所控制着，这种控制可能是直接控制，也有可能是间接控制。
  - 原则：在React开发中，建议使用受控表单，不建议使用“非受控表单”。
  - 单向绑定：通常指的是表单手动取值，对比的是Vue中表单的双向绑定。用受控表单的方式进行取值，这就是单向绑定。
  - 对普通文本框、下拉框、多行文本行，使用 value + onChange 进行受控和取值。
  - 对单选按钮组、复选按钮组件，使用 checked +　onChange 进行受控和取值。
  - 表单取值技巧：当一个页面中有多个表单时，一般不要封装多个 onChange的事件处理器，建议复用表单的事件处理器。
  - 文件上传（input=file），是不用受控的，也就是说文件数据流不需要放到声明式变量上。文件上传，是唯一的一个推荐使用非受控方式的表单。

- 十、状态提升

  - 概念：当我们需要在多个组件之间共享一个变量时，我们通常的做法是把这个变量定义这些组件共同的父级组件的state中，再通过props传递给他们。这种通信方案，就叫做状态提升。
  - 意义：状态提升，在组件化开发中，是最为基础的、也是使用最为频繁的一种通信方案，所以大家要非常熟练。
  - 语法基础：是父子组件之间通信。父组件向子组件传递数据，使用props属性（自定义属性）；子组件向父组件传递数据，使用props事件（自定义事件）。

- 十一、组合（组件化开发）

  - 概念：React 有十分强大的组合模式。我们推荐使用组合而非继承来实现组件间的代码重用。
  - 意义：在React开发中，当我们设计UI视图（这个视图可能是页面、也可能是一个具体可复用的组件）时，首先思考如何拆分这个视图，然后分门别类地把这个拆分的小组件封装起来，最后借助props把这个封装好的组件组合起来。这个思考过程、代码过程，就是所谓的组合。所以，当我们搭建页面时要用到组合；当我们封装组件时，也要用到组合。
  - 语法基础：prop.children、render props。
  - 属性穿透：`<A {...this.props} />`，把父组件传递过来的props，全部给 A组件。
  - Portals：提供了一种将子节点渲染到存在于父组件以外的 DOM 节点的优秀的方案。
    - 语法：ReactDOM.createPortal(<jsx/>, domContainer)
    - 场景：常用于封装Modal弹框组件。
  - 简单谈一谈使用继承，如何封装组件？（不推荐使用继承实现组件化）
    ```
    class Modal extends PureComponent {}
    class ConfirmModal extends Modal {}
    class DangerModal extends Modal {}
    class SmallConfirmModal extends ConfirmModal {}
    class SamllDangerModal extends DangerModal {}
    ```
  - 引申理解：我们在数学中学过“组合”，Head(2种) * Body(2种) * Foot(3种) = Modal(12种)
  - 方法论：如何拆分视图 -> 定义小组件 -> 设计props进行组装。

- 十二、上下文

  - 提出问题：当我们在复杂组件树之间需要传递数据时，使用props一层一层地传递，是非常麻烦的，并且不容易实现。怎么办？使用上下文即可解决问题。
  - 实现方案：使用React.createContext()创建上下文对象，使用<Provider>组件向组件树中注入数据，在后代组件中使用<Consumer>消费数据。
  - 上下文的特点：数据只能自上而下地传递，也就是从能根级父组件向后代组件传递数据。

  - 创建上下文语法：`const ThemeContext = React.createContext()`
  - 怎么向组件树中注入数据：`<Provider value={data}><jsx/></Provider>`
  - 后代组件怎么使用上下文：
    - 1、`Page.contextType=ThemeContext`，在this.context访问上下文数据；
    - 2、`return (<Consumer>{ ctx => (<jsx/>)}</Consumer>)`，借助props.children来使用上下文数据。

  - 重申上下文的三个特点：自上而下（单向数据流）、具有响应式、默认只有类组件才有上下文。
  - 应用场景：在很多第三方的React包中，都用到了上下文，比如路由系统、国际化、Mobx、Redux都用到了上下文。

- 十三、高阶组件

  - 概念：HOC(Higher Order Component)，本质上是一个纯函数（不能直接修改入参），它接收一个UI组件作为入参，返回一个新的UI组件。高阶组件，也被称为高阶函数、容器组件。
  - 作用：用于代码逻辑的复用，用于“修饰”我们的React组件。被修饰的React组件，被称为“UI组件”。
  - 定义高阶组件的注意事项
    - 在高阶组件中不能直接修改UI组件
    - 为避免props丢失问题，一定要使用“属性继承”语法保留props。
    - 被返回的新组件，可以是类组件，也可以是函数式组件。
    - 常见语法一：(Wrapped) => (props) => (<Wrapped {...props} />)
    - 常见语法二：(args) => (Wrapped) => (props) => (<Wrapped {...props} />)
  - 使用高阶组件的两种语法：函数调用 hoc(Home)，在class类组件上使用装饰器语法。
  - 应用场景：复用相同的视图组件、添加公共的属性或方法、实现细粒度的权限管理等等。


- 十四、Hooks编程

  - 趋势：现在市场中React呈现的Hooks编程，几乎都封装函数式组件，类组件越来越少。
  - 概念：自React(v16.8)新增了一套Hooks API，在不影响函数式组件性能的基础上，“模拟”出类组件中的那些特性（生命周期、state、ref、上下文等）。从某种程度上说，Hooks还是更加高级的代码复用技巧。

  - 1、useState
  - 作用：用于在函数式组件中定义声明式变量；这些声明式变量保存在React底层中，不会随着函数式组件的重新运行而被重置。
  - 语法：`const [a, setA] = useState(初始值)`
  - 思考：为什么这里用的是数组解构、而不是对象解构？为什么这里用`const`来定义变量？
  - a 是声明式变量；setA是专门用于修改a的方法，当setA修改a时，会触发当前函数式组件重新执行，返回新的Fiber，进一步执行协调运算，更新DOM视图。
  - 在合成事件中，setA是异步的；在同一个合成事件中执行多次set*方法，会自动合并成一次，只触发一次函数式组件更新。
  - setA 方法，虽然是异步的，但没有callback回调；setA有两种写法：setA(newA),或者setA(a=>(newA))。

  - 2、useEffect
    - 作用：用于在函数式组件中执行一些副作用（定时器、BOM操作、DOM操作、调接口、建立订阅、初始一个第三方插件等）。
    - 理解：useEffect相当于类组件中的 componentDidMount + componentDidUpdate + componentWillUnmount。
    - 语法：`useEffect(()=>{ return ()=>{}}, [依赖列表])`。    
    - 详细解读useEffect的执行流程：`useEffect(function(){ fn1(); return fn2}, [依赖列表])`
      - 当没有第二个参数时。初始化时，只执行fn1()；当set*发生时，先执行fn2()，再执行fn1()；路由切换时，只执行fn2()。
      - 当有第二个参数，但是空数组时。初始化时，只执行fn1()；当set*发生时，什么都不执行；路由切换时，只执行fn2()。
      - 当有第二个参数，且数组中有依赖列表时。初始化，只执行fn1()；当set*更新依赖列表中任何一个声明式变量时，先执行fn2()，再执行fn1()；当set*修改的声明式变量不在依赖数组中时，什么都不执行；路由切换时，fn2()。
    - 两个基本的使用原则
      - 当你需要执行副作用时，不要在函数式组件中直接编写逻辑，而应该使用useEffect将其包裹起来，更精确地控制它的执行逻辑。不要把副作用直接“裸露”在函数式组件的顶层作用域中。
      - 在同一个函数式组件中，可以使用多个useEffect()，需要注意的是一个useEffect只建议做一件事。
    - useLayoutEffect 也是用于执行各种副作用的，但执行时机比useEffect更早，工作中很少用。

  - 3、useContext
    - 作用：在函数式组件中使用上下文数据。
    - 语法：const ctx = useContext(ThemeContext)

  - 4、useReducer（**）
    - 作用：在函数式组件中，模拟Redux数据流，是useState的一种替代方案。
    - 语法：`const [state, dispatch] = useReducer(reducer, initState)`
    - 其中，`dispatch` 相当于class类组件中`this.forceUpdate` 的功能。
    - 提示：Web前端开发中很少使用，在一些第三库中用得比较多，比如react-redux这个库就用到了useReducer。

  - 5、useCallback
    - 作用：在函数式组件中，用于缓存一个函数声明。
    - 语法：`const handler = useCallback(originalHandler, [依赖数组])`
    - 注意：当originalHandler中使用到声明式变量时，必须将其添加到依赖数组中，否则函数体中只能读取声明式变量的旧值。
    - 提示：工作中它用得比较少，面试经常被问到。
    - `useCallback(fn, deps)` 相当于 `useMemo(() => fn, deps)`

  - 6、useMemo
    - 作用：在函数式组件中，用于缓存一个复杂的计算。它相当于Vue中的computed计算属性。
    - 语法：`const expensiveValue = useMemo(computedFn, [依赖数组])`
    - 特点：有且仅有当依赖数组中的声明式变量发生变化时，computedFn才会重新执行、计算返回新的结果。
    - 提示：工作中用得非常多，是一种非常重要的优化功能。

  - 7、useRef
    - 作用：在函数式组件中，使用ref快速访问DOM对象。
    - 语法：`const box = useRef(null)`；`<div ref={box} />`
    - 使用：box.current 代表就是那个DOM对象。
    - 扩展：如何希望ref作用在自定义组件上，我们要借助forwardRef()来实现，一般在第三方UI组件库中用得比较多。

  - 8、useDeferredValue
    - 作用：充分利用React的并发模式特性，自动帮助我们延迟一个声明式变量的更新。相当于防抖功能。和防抖的区别在于，useDeferredValue延迟的时间是不确定的，这得看浏览器进程的“脸色”（看浏览器忙不忙）；而防抖的延迟时间是写死的。
    - 语法：`const deferredValue = useDeferredValue(value)`
    - 举个例子：当我们在搜索框中快速输入文字进行搜索时，我们不能用搜索框的实时值来调接口，而应该使用useDeferredValue把搜索框中的实时值“复制”一份，再用这个复制而来的“副本”去调接口执行搜索。

  - 9、useTransition
    - 作用：用于把一个副作用或set*操作标记成“可中断的”。
    - 语法：`const [isPending, startTransition] = useTransition()`
    - 标记“可中断的”语法：`startTransition(()=>{ do something })`
    - 举个例子：当我们执行查询操作时，从后端调接口拿到列表数据，我们通常的做法是 setList(res.list)，同步地commit提交DOM更新；事实上，我们可以借用 startTransition 把setList(list)标记成“可中断的”，让浏览器可以多一些可能去执行其它更加紧急的任务，比如input输入、动画等。

  - 10、自定义封装Hooks
    - 概念：使用已有的Hooks(官方Hooks或者第三方Hooks)封装我们需要的可复用的功能逻辑。自定义Hooks是一种比较高级的代码逻辑复用技巧。需要注意的是，Hooks并不是高阶组件的替代物，Hooks和高阶组件是两个完全不同的概念。
    - 原则：自定义Hooks的命名必须以 use* 开头；它本质上一个函数，理论上在自定义Hooks的函数内部，你可以做任何事；当自定义Hooks被复用时，它们是完全相互独立的，不必考虑彼此之间的作用域影响。
    - 分享一个常识：如何识别一个函数是不是有效的Hooks呢？第一，看它的命名是否以use*开头；第二，看它的内部逻辑中是否有其它Hooks逻辑。
    - 推荐两个开源的Hooks库：react-use、ahooks（这两个库工作中用得非常多）。
    - react-use文档：https://streamich.github.io/react-use/
    - ahooks文档：https://ahooks.js.org/zh-CN

# 路由系统（V6路由）

  - V5文档（公司中常用）：https://v5.reactrouter.com/
  - V6文档（未来的版本）：https://reactrouter.com/
  - 说明：在`react-webpack`这个项目中，我们使用V6来设计路由系统。
  - 安装：`cnpm i react-router-dom -S`
  - 核心代码，请参见：App.jsx、layout文件夹、view/index.jsx 这三个文件。

  - 罗列若干值得大家注意的路由知识点：
  - 1、两种路由模式：<BrowserRouter> <HashRouter>，这两种路由模式的差异，部署404问题怎么解决。
  - 2、声明式路由：<Link to> <NavLink style to> 最终渲染成a标签，用在菜单设计上。
  - 3、<Navigate> 自动发生跳转，它背后使用的是编程式路由API进行跳转的。
  - 4、<Outlet> 当路由规则匹配成功时所对应的element元素要显示的地方（类比如vue-router中的视图容器）。
  - 5、<Routes> 用于指定一组可以嵌套的路由规则，有助于路由规则的快速匹配和精准匹配（相当V5中的<Switch>）。
  - 6、useLocation() 用于返回路由信息（相当于vue-router中的$route）。
  - 7、useNavigate() 用于编程导航，用代码的方式跳转路由：navigate('/jsx', {replace:true})。
  - 8、useParams() 用于接收“动态路由”的路由参数。
    ```
    <Route path='/good'>
      <Route path='list' element={<GoodList/>} />
      <Route path='/detail/:id' element={<GoodDetail/>} />
    </Route>
    ```
  - 9、useRoutes([...routes]) 使用特定的路由数组一步生成路由规则（V5中没有这个，大家可以研究）。
  - 10、useSearchParams() 用于接收当前URL中的`?a=1&b=2`的Query查询参数。
  - 11、代码分割（性能优化）
    - 为什么要配置代码分割？（相当于vue-router中路由懒加载）
    - 怎么实现？（怎么支持动态导入语法？怎么集成React.lazy()和<Suspense>？推荐使用`@loadable/component`）
    - 代码分割参考文档：https://v5.reactrouter.com/web/guides/code-splitting

# 状态管理工具

  - 意义：对一个中大型的Web项目来讲，如果数据流管理有缺陷，几乎必死。
  - 作用：用于Web应用程序中的数据管理，其一是数据在组件之间共享，其二是实现数据缓存。在中小型项目中建议使用mobx，如果是大型项目推荐使用redux。
  - mobx(v6) 用于定义状态容器（state数据、action方法）
    - 常用API：makeAutoObservable、[makeObservable、observable、action、computed、flow]。
    - makeAutoObservable 用于自动地把store的成员属性变成observable变量，把成员方法变成action方法。
    - makeObservable 用于手动修饰成员属性和成员方法，根据需求把它们变成observable变量或action方法。
    - computed 用于把一个成员方法变成计算属性，配合get关键字使用。
    - flow 用于把一个异步的成员方法变成同步方法。
  - mobx-react(v7) 是基于上下文、高阶组件封装的连接库，用于把Mobx数据容器和React连接起来。
    - 常用API：<Provider {...store}>、inject([])、observer
    - <Provider> 用于把store数据注入到组件树中去。
    - inject() 用于在组件中使用上下文中的store数据。
    - observer 用于把组件修饰成观察者，当上下文中的store数据发生变化时，组件自动更新。

  - 简述Mobx数据流：在生命周期钩子/副作用/交互事件，触发Action调接口、拿到后端数据后将其更新到Observable变量上，再通过<Provider>把数据注入到组件树中，在组件中使用observer把组件变成观察者，即可inject得到Mobx最新数据，组件自动渲染更新。（Mobx四个概念：Observable、Action、Reaction、Computed）

# create-react-app
  - 官方文档：https://create-react-app.dev/
  - 全局安装脚手架（注意node版本必须v14以上）
    ```
    cnpm i create-react-app -g
    cnpm i yarn -g
    create-react-app --version
    ```
  - 创建项目、运行项目
    ```
    create-react-app react-project
    # OR
    yarn create react-app react-project
    cd react-project
    npm start
    ```    
  - 执行“暴露”操作（得到Webpack文件）
    ```
    git init
    git add --all
    git commit -m 'first commit'
    npm run eject  
    # 提示你输入 y
    # 等待项目结构“暴露”成功后，在命令行“Ctrl+C”结束进程
    # 手动删除项目根目录中的node_modules包
    # 再用 yarn 或 镜像源安装相关依赖
    cnpm install
    npm start
    ```
  - 整理项目目录：删掉一些无用的文件，关闭reportWebVitals功能，根据公司需求设计目录结构。
  - 修改webpack若干配置
    - 改端口 scripts/tart.js  DEFAULT_PORT
    - 配代理 scripts/tart.js
      - 推荐做法：src/setupProxy.js 编写跨域代理的代码（参考create-react-app官网）
      - 在package.json 添加 "proxy"字段，编写代理配置
      - 在config/webpackDevServer.config.js中直接编写代理的代码

    - 关闭自动打开浏览器 scripts/tart.js  openBrowser
    - 修改入口文件名  config/paths.js  appIndexJs
    - 添加@别名 config/webpack.config.js  alias
    - 添加less编译： config/webpack.config.js  ** STOP ** Are you adding a new loader?
    ```
    {
      test: /\.less$/i,
      use: [
        isEnvDevelopment ? 'style-loader' : {
          loader: MiniCssExtractPlugin.loader,
          options: paths.publicUrlOrPath.startsWith('.')
            ? { publicPath: '../../' }
            : {},
        },
        "css-loader",
        "less-loader"
      ],
    }
    ```
  - 安装AntDesign组件库    
    - cnpm i antd -S
    - 在项目根目录添加babel.config.js，配置Babel插件
    - 在public/index.html添加antd.css的CDN链接

# react-router-dom(V5)
  ```
  - cnpm i react-router-dom@5.3.1 -S
  - cnpm i @loadable/component -S
  - cnpm i @babel/plugin-syntax-dynamic-import -D
  ```
  - <HashRouter> / <BrowserRouter>
  - <Switch> / <Route> / <Redirect>
  - <Link> / <NavLink>
  - withRouter
  - useHistory / useLocation / useParams
  - 代码分割 `@loadable/component`

# redux状态管理工具

 - Flux单向数据流：https://facebook.github.io/flux/
  - [State -> View -> Actions]

 - redux：用于定义数据容器。

  - 一个流程(会画、会说、会写)
  - 三个API（createStore、applyMiddleware、combineReducers）
  - 三个概念(store、reducer、action)
  - 三个特点(单一数据源、只读、只能使用纯函数reducer修改)

 - react-redux 用于连接Redux数据容器和React组件树，它是基于上下文和高阶组件而封装的。
  - <Provider store={store}>
  - connect(mapStateToProps, mapDispatchToProps)
  - useSelector(state=>state.good) 在函数式组件中访问store中的state数据
  - useDispatch() 在函数式组件中得到dispatch这个API
  
 - redux-logger 用于调试reducer数据流更新前与更新后的状态。
 - redux-thunk  让redux支持接收一个函数作为dispatch的参数。
 - redux-saga   是redux-thunk的替代方案，用于处理redux异步数据流问题，在umi中我们再讲。
 - @reduxjs/toolkit  是redux官方提供的一种兼容TS环境的Redux架构。

# 项目实战重点

  - 使用react-router-dom(V5) 如何实现登录鉴权、权限设计？
  - 尽可能地把 ant-design 组件都使用一遍，翻一遍文档也行
  - 熟练掌握 redux 数据流，实现增删改查的常规功能
  - 熟练掌握 Hooks 编程技巧，区分类组件编程、Vue开发的差异
  - 扩展功能实现：国际化、更换主题色、使用第三方组件库（比如富文本编辑器）、图表开发、地图开发等
  - 等等

# Umi 脚手架

  - umi项目创建，参考umi文档。
  - umi是基于配置的，有很功能都是配置出来。除了编写业务逻辑外，其它的功能几乎都配置出来的。
  - .umirc.ts 这是umi脚手架的其中一种配置方式，还可以使用config目录来配置（在and-design-pro中可以看到config配置方式）。
  - .umirc.ts 这个配置文件是支持热更新的，你添加配置成功，浏览器中的代码会自动更新，无须重启。
  - umi项目，是基于插件的。umi背后有很多umi插件，比如: @umijs/* 。不同的umi插件有不同的功能。
  - umi内置了路由功能，有两种路由模式，一种约定式路由，一种配置式路由。
  - umi内置了dva（redux、redux-saga），umi中默认就是支持redux状态管理。
  - 提示：为了更好学习Umi和TS，建议大家创建一个ant-design-pro的项目玩一下。

# TS 入门学习

  - TS是微软发布的JavaScript超集，与脸书的Flow竞争的。Vue2是用Flow语言编写的，Vue3是用TS编写的。Vue3的兴趣，也带动了TS在国内的兴趣。（2020年底）
  - tsconfig.json 是TS的配置文件，配置选项非常多。
  - XXX.d.ts 这是TS的声明文件，用于指定环境中TS模块和变量的声明。
  - @types/XXX，这些包，是第三方提供的声明文件。
  - .ts 是文件后缀，TS环境只会检测.ts/.tsx这种文件，对.js/.jsx文件不起使用。如果你要写TS代码，请把文件命名成.ts/.tsx文件。
  - TS语法范围 > JavaScript语法范围。TS中有很语法，在ES6+中开始逐步实现了。
  - 学TS学什么呢？学习TS类型、学习TS面向对象编程。对咱们来讲，只用学TS类型，在Vue/React开发环境中主要使用的是TS类型校验。
  - TS类型：字符串、数值类型、布尔类型、数组类型、对象类型、void类型、never类型、元组、枚举等。
  - 提示：为了更好地编写TS代码，建议使用VsCode编辑器。VsCode和TS都是微软这家公司的产品，VsCode已经内置了TS检测插件，对TS有着更加友好的支持。
  - TS入门笔记：https://zhuanlan.zhihu.com/p/377754481
