```
function fetch(url,method,data) {
  return new Promise(resolve=>{
    $.ajax({
      url,
      data,
      method,
      success: (res) => {
        resolve(res)
      }
    })
  })
}

async mounted () {
  const res = await fetch(url,'GET',data)
}
```
